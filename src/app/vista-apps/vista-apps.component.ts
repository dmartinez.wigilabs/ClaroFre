import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params  } from '@angular/router';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import * as Highcharts from 'highcharts';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { DatePipe } from '@angular/common';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');
declare var google: any;

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);



@Component({
  selector: 'app-vista-apps',
  templateUrl: './vista-apps.component.html',
  styleUrls: ['./vista-apps.component.css']
})
export class VistaAppsComponent implements OnInit {
  name: any;
  private map: google.maps.Map = null;
  private heatmap: google.maps.visualization.HeatmapLayer = null;
  public appName = name;
  public Apps;
  public tipo;
  public datos = [];
  public datosIn = [];
  public datosUh = [];
  public totales = [];
  public fechain;
  public datosmap = [];
  totalUsers=0;
  totalClicks=0;
  totalDownloads=0;
  totalname=0;
  totalcant = 0;
  public max = new Date();
  submitted = false;
  FeFormRappi: FormGroup;
  public fechaAct = new Date()
  public fechaAc = new Date().getTime();
  public DIA_EN_MILISEGUNDOS = 24 * 60 * 60 * 1000;
  ayer : String;
  newAc : String;
  latitude ;
  longitude ;
  lat = 4.6486259;
  lng = -74.2478958;
 

  public options: any = {
    chart: {
      // Editar chart espacio
      spacingBottom: 10,
      spacingTop: 20,
      spacingLeft: 20,
      spacingRight: 20,
    },
    title: {
      text: "Usuarios periodo",
      align: 'left',
    },
    credits: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    xAxis:{
      type: 'datetime',
      tickInterval: 24 * 3600 * 1000,
      dateTimeLabelFormats: {
          day: '%e-%b',
      }
    },
    yAxis: {
      min: 1,
      tickInterval: 1,
      title: {
          text: ''
      },
      labels: {
        formatter: function () {
            return  this.axis.defaultLabelFormatter.call(this) + '';
        }            
      }
    },
    plotOptions: {
      series: {
        color: '#E93030'
      }
    },
    tooltip: {
      valueSuffix:""
    },
    series: [
       {
        name: 'usuario',
        data: [ ]
       },
    ]
  } 
  public spline: any = {
    
    chart:{
      type: 'spline',
      spacingBottom: 0,
      spacingTop: 0,
      spacingLeft: 15,
      spacingRight: 15,
    },
    title: {
      text: "Clics App/Hora",
      align: 'left',
    },
    plotOptions: {
      column: {
        pointPadding: 0,
        borderWidth: 0,
        groupPadding: 0,
        shadow: false
      },
      series: {
        pointWidth: 35,
        color: '#E93030'
      }
    },
    credits: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    xAxis:{
      type: 'datetime',
      tickInterval: 24 * 3600 * 1000,
      dateTimeLabelFormats: {
          day: '%e-%b',
      }  
    },
    yAxis: {
      min: 1,
      tickInterval: 1,
      title: {
        text: ''
      },
    },
    series: [
      {
        name: 'click',
        data: [
          
        ],
      },
    ]
  }
  constructor(private route: ActivatedRoute,  private router : Router, private apiService : ApiService, private spinner: NgxSpinnerService,  private formBuilder: FormBuilder,private datePipe: DatePipe,  dateTimeAdapter: DateTimeAdapter<any>) 
  { Highcharts.setOptions({
    lang: {
        loading: 'Cargando...',
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        thousandsSep: ",",
        decimalPoint: '.',

    },
    time: {
      useUTC: false
    }
  });
}

  ngOnInit() {
    let date = this.datePipe.transform(new Date(this.fechaAct),"yyyy-MM-dd").split('-');
    let dia = parseInt(date[2]) - 1;
    let dateYesterday = new Date(date[0] + "-" + date[1] + "-" +  dia);

    this.newAc = this.datePipe.transform(new Date(this.fechaAct),"yyyy-MM-dd"); 
    this.ayer =  this.datePipe.transform(dateYesterday,"yyyy-MM-dd");
    this.route.paramMap.subscribe(params => { 
    this.name = params.get('name');  
  });
  this.options =Highcharts.chart('container3', this.options); 
  this.spline = Highcharts.chart('container4', this.spline);
  //------------------------------------------------------clics_x_hora----------------------------------------------
    this.FeFormRappi = this.formBuilder.group({
      desde:['',[
        Validators.required
      ]],
      hasta: ['',[
        Validators.required
      ]], 
    })    
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    if (navigator.geolocation) {
      // navigator.geolocation.getCurrentPosition(displayLocationInfo);
      this.latitude = -74.2478936;
      this.longitude = 4.6486259;
    }else{
      this.latitude = -74.2478936;
      this.longitude = 4.6486259;
    }
    function displayLocationInfo(position) {
      this.location = { 
        latitude: position.coords.latitude,
        longitude: position.coords.longitude
      };
    }

    this.spinner.show();
    this.consuptionServiceDatosin();
    this.consuptionServiceDatosUh();
    this.consuptionServicetotales();
    
    
  }

  consuptionServiceDatosin(){
    this.Apps = 'clics_x_hora';
    this.apiService.postGetInfo(this.ayer, this.newAc, this.Apps, this.name ).subscribe((res) => {
      if (res.error == 1) {
        alert('error');     
      }else{
        this.datosIn = res.response; 
        this.spinner.hide();
        let dataListClicks = [];
        for (let i = 0; i < this.datosIn.length; i++) {
          let infoCli = [ parseInt(this.datosIn[i].name), this.datosIn[i].cant];
          dataListClicks.push(infoCli);
        }
        this.spline.update({
          series: [{
            data: dataListClicks
          }]
        })
      }
    }
  )
  }
  consuptionServiceDatosUh(){
    this.Apps = "users_x_hora";
    this.apiService.postGetInfo(this.ayer, this.newAc, this.Apps, this.name ).subscribe((res) => {
      if (res.error == 1) {
        alert('error');     
      }else{
        this.datosUh = res.response; 
        this.spinner.hide();
        let dataList = [];
        for (let i = 0; i < this.datosUh.length; i++) {
          let info = [parseInt(this.datosUh[i].name),this.datosUh[i].cant];
          dataList.push(info);
        }
        this.options.update({
          series: [{
            data: 
              dataList
          }]
        })
      }
    }
    )
  }
  consuptionServicetotales(){
    //------------------------------------------------------totales----------------------------------------------
  this.Apps = "totales";
  this.apiService.postGetInfo(this.ayer, this.newAc, this.Apps, this.name ).subscribe((res) => {
    if (res.error == 1) {
      alert('error');     
    }else{
      this.totales = res.response;
      this.spinner.hide();
      for (let index = 0; index < this.totales.length; index++) {
        const element = this.totales[index];
        switch (element.name) {
          case 'users':
            this.totalUsers=element.cant;
            break;
          case 'click':
            this.totalClicks=element.cant;
            break;
          case 'appDownload':
            this.totalDownloads=element.cant;
            break;
          default:
            break;
        }
        
      }
    }
  }
  )
  }
  obtenerinfomap(){
    
  }
  onMapLoad(mapInstance: google.maps.Map) {
    this.map = mapInstance;
    this.Apps = "mapa";
    this.apiService.postFechas(this.ayer, this.newAc, this.Apps ).subscribe((res) => {
      this.spinner.hide();
      if (res.error == 1) {
        alert('error');     
      }else{
        this.datosmap = res.response;
        console.log(this.datosmap)
        let placesMap = [];
        for (let i = 0; i < this.datosmap.length; i++) {
          placesMap.push(new google.maps.LatLng(this.datosmap[i].pos.lat,this.datosmap[i].pos.lng));
        }
        //var sanFrancisco = new google.maps.LatLng(this.latitude, this.longitude);
        /*map = new google.maps.Map(document.getElementById('MapaInteracciones'), {
          center: sanFrancisco,
          zoom: 13,
          mapTypeId: 'satellite'
        });*/
        /*var heatmap = new google.maps.visualization.HeatmapLayer({
          data: placesMap
        });
        heatmap.setMap(map);*/
        const coords: google.maps.LatLng[] =  placesMap; // can also be a google.maps.MVCArray with LatLng[] inside    
        this.heatmap = new google.maps.visualization.HeatmapLayer({
        map: this.map,
        data: coords
        });
      }
    }
    )
    // here our in other method after you get the coords; but make sure map is loaded

}
  //----------------------------------------------------------------------------------------------
  

  get f(){ return this.FeFormRappi.controls }

  onSubmit(){
   
    this.submitted = true;
    if(this.FeFormRappi.invalid){
      return;
    }else{
      this.FeFormRappi.value.desde = this.newAc;
      this.FeFormRappi.value.hasta = this.ayer;
      this.spinner.show();
      this.consuptionServiceDatosin();
      this.consuptionServiceDatosUh();
      this.consuptionServicetotales();
      this.obtenerinfomap();
    }
    this.submitted = false;
  }
 
}