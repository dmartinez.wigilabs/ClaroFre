import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RappiComponent } from './rappi/rappi.component';
import { TodasComponent} from './todas/todas.component';
import { VistaAppsComponent } from './vista-apps/vista-apps.component'
import { AuthGuard } from './authGuard/auth.guard'


const routes: Routes = [
    { 
      path:'',
      redirectTo: '/inicio',
      pathMatch: 'full'
    },
    {
      path: 'inicio',
      component: InicioComponent
    },
    {
      path: 'dashboard',
      component: DashboardComponent,
      /*canActivate: [AuthGuard]*/
      children: [
        //UserInfo component is rendered when /users/:id is matched
          { path: 'app/:name', component: VistaAppsComponent }
        ]
    },
    {
      path: '**', redirectTo: '/inicio' 
    },
];
const routeOptions: ExtraOptions = {
  useHash: false,
  anchorScrolling: 'enabled',
  scrollPositionRestoration: 'enabled'
}

@NgModule({
  imports: [RouterModule.forRoot(routes, routeOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
